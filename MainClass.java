package assignment5_tasks;

public class MainClass {
	private final static String defaultActivitiesFile = "Activities.txt";
	private final static String defaultFile = "occurancesOfEachActiv.txt";
	private final static String defaultFile3 = "activDuration.txt";
	private final static String defaultFile4 = "activDurStream2.txt";
	private final static String defaultFile5 = "filter.txt";
	private final static String defaultFile6 = "filterNew.txt";
	
	public static void main(String[] args) {
		MonitoredData file = new MonitoredData();
		file.loadActivities(defaultActivitiesFile);

		file.countNrDaysMonitored(file.getListAc());
		System.out.println("\nNumber of days monitored: " + file.getNrDaysM() + "\n");
		file.countNrDaysMonitored2(file.getListAc());
		System.out.println("\nNumber of days monitored: " + file.getNrDaysM2() + "\n");

		file.countNrActivities(file.getListAc());
		file.seeHashMapStringInt(file.gethashMapactivCount(), defaultFile);

		file.ActivCountForEachDay(file.getListAc());
		file.seeActivCountForEachDay();

		System.out.println("\ntested addition and substraction with landda");
		file.testAdd();

		file.activDurationLarger(file.getListAc());
		file.see4(file.getActivDur(), defaultFile3);

		file.filterActivDurationLarger(defaultFile4);

		file.filter();
		file.viewFilterLast(defaultFile5);
		
		file.tryFilNew(defaultFile6);


	}
}
