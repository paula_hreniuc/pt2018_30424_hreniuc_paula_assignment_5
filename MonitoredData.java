package assignment5_tasks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {

//	private final static String defaultActivitiesFile = "Activities.txt";
//	private final static String defaultFile = "occurancesOfEachActiv.txt";
	private final static String defaultFile2 = "activForEachDay.txt";
//	private final static String defaultFile3 = "activDuration.txt";
//	private final static String defaultFile4 = "activDurStream2.txt";
//	private final static String defaultFile5 = "filter.txt";


	private List<MonitoredData> data;
	private List<MonitoredData> data2;
	private HashMap<String, Integer> activCount;
	private HashMap<String, LocalDateTime> activDuration;
	private HashMap<String, LocalDateTime> activDurationUnaltered;
	private HashMap<Integer, HashMap<String, Integer>> activForEachDay;
	private List<String> activFiltered ;
	private static HashMap<String, Integer> act;
	private HashMap<String, Integer> actUnder5;

	String startTime;
	String endTime;
	String activity;
	int count = 0; //counting the nr of monitored days
	int count2 = 0;
	int day = -1; //fist day to compare with 

	@FunctionalInterface
	public interface Adder{
		int add (int n1, int n2);
	}

	@FunctionalInterface
	public interface Substracter{
		int sub(int n1, int n2);
	}

	public void testAdd() {
		Adder sum = (x, y) ->x +y;
		Substracter minus = (x, y)->x-y;
		System.out.println(sum.add(2, 3) + " " + minus.sub(10, 1));
	}	

	public MonitoredData (){

	}

	public MonitoredData (String startT, String endT, String activity){
		this.startTime = startT;
		this.endTime = endT;
		this.activity = activity;
	}

	public MonitoredData SplitALine( String line) {
		String date1 = null;
		String date2 = null; 
		String activity = null;
		int i = 0;

		for (String retval: line.split("		")) {
			if(i == 0) {
				date1 = retval;
				i++;
			}
			else if (i == 1) {
				date2 = retval;
				i++;
			}
			else if(i == 2) {
				activity = retval;
			}
			//System.out.println(retval);
		}
		MonitoredData newLine = new MonitoredData(date1, date2, activity);
		return newLine;
	}

	public boolean loadActivities(String filename) {
		try {
			FileInputStream fileStream = new FileInputStream(filename);
			DataInputStream in = new DataInputStream(fileStream);
			InputStreamReader inr = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(inr);
			data = new ArrayList();

			String strLine;
			while ((strLine = br.readLine()) != null) {
				System.out.println(strLine);
				MonitoredData md =SplitALine(strLine);
				System.out.println(md.getStartTime()+ "\t\t" + md.getEndTime() + " \t\t" +md.getActivity());
				data.add(md);
			}
			br.close();
			inr.close();
			in.close();
			fileStream.close();
			//assert (isConsistent());
			return true;
		} catch (Exception e) {
			System.out.println("Error reading from file");

			return false;
		}
	}


	public int getDay(String startTime) {
		int day= -1;
		int i = 0;
		Adder adder = (x, y) ->x +y;

		//Split the starting time
		String date = null;
		String hourT;

		for (String retval: startTime.split(" ")) {
			if(i == 0) {
				date = retval;
				//i++;
				i = adder.add(i, 1);
			}
			else if (i == 1) {
				hourT = retval;
			}
			//System.out.println(retval);
		}

		//get day from the date
		int j = 0;
		for (String retval: date.split("-")) {
			if(j == 2) {
				day = Integer.parseInt(retval);
			}
			//j++;
			j = adder.add(j, 1);
		}

		return day;
	}

	public void countNrDaysMonitored(List<MonitoredData> dataA) {
		int foundDay;

		Iterator<MonitoredData> iterator = dataA.iterator();
		while (iterator.hasNext()) {
			String st = iterator.next().getStartTime();
			foundDay = getDay(st);
			if(day != foundDay) {
				//count ++;
				Adder adder = (x, y) ->x +y;
				count = adder.add(count, 1);
				day = foundDay;
			}
			System.out.println(st);
		}
	}

	public void countNrDaysMonitored2(List<MonitoredData> dataA) {
		int foundDay;
		Stream<MonitoredData> listStream = dataA.stream();
		//System.out.println(dataA.stream().filter(e->e.getActivity() != "").map(e->e.getActivity()));
		
		Iterator<MonitoredData> iterator = listStream.iterator();
		while (iterator.hasNext()) {
			String st = iterator.next().getStartTime();
			foundDay = getDay(st);
			if(day != foundDay) {
				Adder adder = (x, y) ->x +y;
				count2 = adder.add(count2, 1);
				day = foundDay;
			}
		}
	}

	public HashMap<String, Integer> countNrActivities(List<MonitoredData> dataA){
		activCount = new  HashMap<String, Integer>();

		Iterator<MonitoredData> iterator = dataA.iterator();
		while (iterator.hasNext()) {
			String ac = iterator.next().getActivity();
			//System.out.println(ac);
			if(!activCount.containsKey(ac)) {
				activCount.put(ac,1);
			}
			else {
				activCount.put(ac, activCount.get(ac)+1);
			}
		}
		return activCount;
	}

	public void seeHashMapStringInt(HashMap<String, Integer> aMap, String fileName) {

		try {

			FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter out = new BufferedWriter(fw);

			Enumeration<String>keys = Collections.enumeration(aMap.keySet());
			while(keys.hasMoreElements()) {
				String ac = (String)(keys.nextElement());
				System.out.println(ac + " "+ aMap.get(ac));
				out.write(ac + " "+ aMap.get(ac));
				out.newLine();
			}
			out.newLine();

			out.close();
			fw.close();
		} catch (Exception e) {
			System.out.println("Error writing to file");
			e.printStackTrace();
		}

	}

	public Map<Integer, HashMap<String, Integer>> ActivCountForEachDay(List<MonitoredData> dataA){
		activForEachDay = new HashMap<Integer, HashMap<String, Integer>>();
		HashMap<String, Integer> myHashMap = null;

		Iterator<MonitoredData> iterator = dataA.iterator();
		while (iterator.hasNext()) {
			int day = getDay(iterator.next().getStartTime());
			String acc = iterator.next().getActivity();
			//System.out.println(day);
			//if the day is different make a new  HashMap<String, Integer>() for that activity with count 1 otherwise increment with one if if is allready created 
			if(!activForEachDay.containsKey(day)) {
				myHashMap = new HashMap<String, Integer>();

				if(!myHashMap.containsKey(acc)) {
					myHashMap.put(acc,1);
				}
				else {
					myHashMap.put(acc, myHashMap.get(acc)+1);
				}

				activForEachDay.put(day,myHashMap);
			}
			else {
				if(!myHashMap.containsKey(acc)) {
					myHashMap.put(acc,1);
				}
				else {
					myHashMap.put(acc, myHashMap.get(acc)+1);
				}
				activForEachDay.put(day, myHashMap);

			}
		}	
		return activForEachDay;

	}

	public void seeActivCountForEachDay () {
		Enumeration<Integer>keys = Collections.enumeration(activForEachDay.keySet());
		while(keys.hasMoreElements()) {
			int day = (Integer)(keys.nextElement());
			HashMap<String, Integer> aED = activForEachDay.get(day);
			System.out.println(day + " ");
			seeHashMapStringInt(aED, defaultFile2);
		}
	}


	public LocalDateTime getTime(String time) {

		LocalDateTime ldt = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));	
		return ldt;
	}

	public LocalDateTime DurationInHours(LocalDateTime start, LocalDateTime end) {
		LocalDateTime duration = null;
		Substracter minus = (x, y)->x-y;
		Adder adder = (x, y) ->x + y;

		Period time = Period.between(start.toLocalDate(),end.toLocalDate());
		int hr, mn = 0 , sc = 0 ;

		int hE =  end.getHour();
		int mnE = end.getMinute();
		int scE = end.getSecond();

		int hS =  start.getHour();
		int mnS = start.getMinute();
		int scS = start.getSecond();

		if(time.getDays() == 1) {
			hE = adder.add(hE, 24);//hE = hE +24;
		}

		if(scE < scS) {
			sc = adder.add(sc ,60 + scE); sc = minus.sub(sc, scS);//sc = 60 + scE -scS;
			if(mnS == 0) {
				mnS = 59;
				hS = minus.sub(hS, 1);//hS --;
			}
			else
				mnS = minus.sub(mnS, 1);//mnS --;
		}
		else {
			sc = minus.sub(scE, scS);//sc = scE- scS;
		}

		if(mnE < mnS) {
			mn = adder.add(mnE, 60); mn = minus.sub(mn, mnS);//mn = 60 + mnE - mnS;
			hE = minus.sub(hE, 1);//hE --;

		}
		hr = minus.sub(hE, hS);//hr = hE - hS;
		
		System.out.println(time.getYears()+ " "  + time.getMonths()+ " "  + time.getDays()+ " "  + hr + " " + mn+ " "  + sc );
		//duration = new DateTime(time.getYears(), time.getMonths(), time.getDays(), hr, mn, sc );
		duration = LocalDateTime.of(1, 1, time.getDays() + 1, hr, mn, sc );

		return duration;	
	}

	public Map<String, LocalDateTime> activDurationLarger(List<MonitoredData> dataA){
		activDuration = new  HashMap<String, LocalDateTime>();
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		int i = 1;

		Iterator<MonitoredData> iterator = dataA.iterator();
		while (iterator.hasNext()) {
			MonitoredData md =  iterator.next();
			String st = md.getStartTime();
			String et = md.getEndTime();
			String a = md.getActivity() + " " + Integer.toString(i);
			i++;

			LocalDateTime startT = getTime(st);
			LocalDateTime endT = getTime(et);			
			System.out.println("\n"+ a+"\n" +format.format(startT) + " " + format.format(endT) );

			LocalDateTime dur = DurationInHours(startT, endT);
			System.out.println(format.format(dur));

			activDuration.put(a, dur);
		}
		return 	activDuration;
	}


	public void filterActivDurationLarger(String fileName){
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		List<String> actNames = activDuration.keySet().stream().collect(Collectors.toList());
		List<LocalDateTime>	result = activDuration.values().stream().collect(Collectors.toList());
		System.out.println("\n" + result + "\n");

		try {

			FileWriter fw = new FileWriter(fileName, true );
			BufferedWriter out = new BufferedWriter(fw);

			Iterator<LocalDateTime> iterator = result.iterator();
			Iterator<String> iterator2 = actNames.iterator();
			while(iterator.hasNext() && iterator2.hasNext()) {
				LocalDateTime date = iterator.next();
				String activ = iterator2.next();
				if(date.getHour() >= 10 || date.getDayOfMonth() == 2) {
					out.write(activ + format.format(date));
					out.newLine();
				}
			}
			out.newLine();
			out.close();
			fw.close();
		} catch (Exception e) {
			System.out.println("Error writing to file");
			e.printStackTrace();
		}		
	}

	public void see4( Map<String, LocalDateTime> Map, String fileName) {

		try {

			FileWriter fw = new FileWriter(fileName, true );
			BufferedWriter out = new BufferedWriter(fw);

			Enumeration<String>keys = Collections.enumeration(Map.keySet());
			while(keys.hasMoreElements()) {
				String act = (String)(keys.nextElement());
				out.write(act + " "+ Map.get(act));
				out.newLine();
			}
			out.newLine();
			out.close();
			fw.close();
		} catch (Exception e) {
			System.out.println("Error writing to file");
			e.printStackTrace();
		}

	}

	public HashMap<String, Integer> countAllAc(List<MonitoredData> dataA){
		act = new  HashMap<String, Integer>();

		Iterator<MonitoredData> iterator = dataA.iterator();
		while (iterator.hasNext()) {
			String ac = iterator.next().getActivity();
			if(!act.containsKey(ac)) {
				act.put(ac,1);
			}
			else {
				act.put(ac, act.get(ac)+1);
			}
		}
		return act;
	}

	public HashMap<String, Integer> countAllActUnder5(List<String> actNames, List<LocalDateTime>result){
		actUnder5 = new  HashMap<String, Integer>();
		String youString = null;

		Iterator<LocalDateTime> iterator = result.iterator();
		Iterator<String> iterator2 = actNames.iterator();
		while(iterator.hasNext() && iterator2.hasNext()) {
			LocalDateTime date = iterator.next();
			String activ = iterator2.next();

			int spacePos = activ.indexOf(" ");
			if (spacePos > 0) {
				youString= activ.substring(0, spacePos );
			}
			System.out.println(activ + youString);/////////////////////////////////

			if(date.getHour() == 0 && date.getDayOfMonth() != 2 && date.getMinute() < 5) {
				System.out.println(date.getMinute() + " " + date.getSecond());
				if(!actUnder5.containsKey(youString)) {
					actUnder5.put(youString,1);
				}
				else {
					actUnder5.put(youString, actUnder5.get(youString)+1);
				}
			}		
		}
		return actUnder5;
	}

	public List<String> filter(){
		List<String> actNames = activDuration.keySet().stream().collect(Collectors.toList());
		List<LocalDateTime>	result = activDuration.values().stream().collect(Collectors.toList());
		activFiltered = new ArrayList<String>();
		HashMap<String, Integer> totalMonitored = countAllAc(data);
		HashMap<String, Integer> lessThan5 = countAllActUnder5( actNames, result);

		System.out.println("\n");
		Enumeration<String>keys = Collections.enumeration(lessThan5.keySet());
		while(keys.hasMoreElements()) {
			String myKey = (String)(keys.nextElement());
			int less5 = lessThan5.get(myKey);
			System.out.println( myKey+ " lessThan5??????????? " + less5);
		}
		System.out.println("\n");
		Enumeration<String>keys2 = Collections.enumeration(totalMonitored.keySet());
		while(keys2.hasMoreElements()) {
			String myKey = (String)(keys2.nextElement());
			int tot = totalMonitored.get(myKey);
			System.out.println(myKey + "Total !!!!!!!!!! " +tot);
		}

		Enumeration<String>keys3 = Collections.enumeration(lessThan5.keySet());
		System.out.println("\nfil");
		while(keys3.hasMoreElements()) {
			String myKey = (String)(keys3.nextElement());
			int less5 = lessThan5.get(myKey);
			int tot = totalMonitored.get(myKey);
			//System.out.println("###"+ myKey + less5/tot + "\n");
			if ( less5/tot >= 0.9) {//less5 >= (90/100) *tot
				activFiltered.add(myKey);
				System.out.println("!!!!!!!!!!"+ myKey + less5/tot + "\n");
			}
		}

		return activFiltered;
	}

	public void viewFilterLast(String fileName){	
		try {
			FileWriter fw = new FileWriter(fileName, true );
			BufferedWriter out = new BufferedWriter(fw);

			//parcurgere lista si write to file
			Iterator<String> iterator = activFiltered.iterator();
			while(iterator.hasNext()) {
				out.write(iterator.next());
				out.newLine();
			}
			out.close();
			fw.close();
		} catch (Exception e) {
			System.out.println("Error writing to file");
			e.printStackTrace();
		}		
	}

	public void tryFilNew(String fileName) {
		List<String> actNames = activDuration.keySet().stream().collect(Collectors.toList());
		System.out.println("\n" + actNames + "\n");
		actNames.forEach(p->{System.out.println(p);});
		
		try {
			FileWriter fw = new FileWriter(fileName, true );
			BufferedWriter out = new BufferedWriter(fw);

			actNames.forEach(p->{try {
				out.write(p);
			} catch (IOException e) {
				e.printStackTrace();
			}try {
				out.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}});
			
			out.close();
			fw.close();
		} catch (Exception e) {
			System.out.println("Error writing to file");
			e.printStackTrace();
		}		
	}

	public String getStartTime() {
		return this.startTime;
	}

	public String getEndTime() {
		return this.endTime;
	}

	public String getActivity() {
		return this.activity;
	}

	public List<MonitoredData> getListAc(){
		return this.data;
	}

	public int getNrDaysM() {
		return this.count;
	}
	public int getNrDaysM2() {
		return this.count2;
	}

	public HashMap<String, Integer>  gethashMapactivCount() {
		return this.activCount;
	}

	public Map<String, LocalDateTime> getActivDur() {
		return this.activDuration;
	}

}
